# Tiancheng Zhu 8/11/2016
# PA2 - 3 postorder.py



def postOrder(lst) :
	if len(lst) == 3 :
		if lst[-1] == '+' :
			return lst[0] + lst[1]
		elif lst[-1] == '-' :
			return lst[0] - lst[1]
		elif lst[-1] == '*' :
			return lst[0] * lst[1]
		elif lst[-1] == '/' :
			return lst[0] / lst[1]
		elif lst[-1] == '%' :
			return lst[0] % lst[1]
	else :
		if lst[-1] == '+' :
			return postOrder( lst [ : ( (len(lst)-1)/2) ] )  +   postOrder( lst [ ((len(lst) - 1)/2) : -1] )
		elif lst[-1] == '-' :
			return postOrder( lst [ : ( (len(lst)-1)/2) ] )  -   postOrder( lst [ ((len(lst) - 1)/2) : -1] )
		elif lst[-1] == '*' :
			return postOrder( lst [ : ( (len(lst)-1)/2) ] )  *   postOrder( lst [ ((len(lst) - 1)/2) : -1] )
		elif lst[-1] == '/' :
			return postOrder( lst [ : ( (len(lst)-1)/2) ] )  /   postOrder( lst [ ((len(lst) - 1)/2) : -1] )
		elif lst[-1] == '%' :
			return postOrder( lst [ : ( (len(lst)-1)/2) ] )  %   postOrder( lst [ ((len(lst) - 1)/2) : -1] )
			
			
print "\n postorder"
post = [7, 7, '*', 3, 4, '+', '*']
print post
print postOrder(post)