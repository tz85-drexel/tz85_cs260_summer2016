# Tiancheng Zhu 8/11/2016
# PA2 - 2 traversalconvert.py

def pretoPost(lst) :
	if len(lst) == 3 :
		tmp = lst[:]
		tmp[0:2] = lst[1:3]
		tmp[2] = lst[0]
		return tmp
	else :
		tmp = lst
		tmp = pretoPost(lst[1:((len(lst)+1)/2)]) + pretoPost(lst[((len(lst)+1)/2):]) + [lst[0]]
		return tmp
		
def posttoPre(lst) :
	if len(lst) == 3 :
		tmp = lst[:]
		tmp[1:3] = lst[0:2]
		tmp[0] = lst[2]
		return tmp
	else :
		tmp = [lst[-1]] + posttoPre( lst[ 0 : ( (len(lst)-1) /2) ] ) + posttoPre( lst[ ( (len(lst)-1) /2) : -1 ] )
		return tmp

def pretoIn(lst) :
	if len(lst) == 3 :
		tmp = lst[:]
		t = tmp[0]
		tmp[0] = lst[1]
		tmp[1] = t
		return tmp
	else :
		tmp = lst
		tmp = pretoIn( lst[ 1 : ( (len(lst)+1) /2) ] ) + [lst[0]] + pretoIn( lst[ ((len(lst)+1) /2) : ] )
		return tmp

Pre = ['A', 'B', 'D', 'H', 'I', 'E', 'J',  'K', 'C', 'F', 'L', 'M', 'G', 'N', 'O']

print "Preorder -> Postorder:"
Post = pretoPost(Pre)
print Post

print "\nPostorder -> Preorder"
pre2 = posttoPre(Post)
print pre2

print "\nPreorder -> - Inorder:"
inorder = pretoIn(Pre)
print inorder