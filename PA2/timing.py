# Tiancheng Zhu 8/11/2016
# PA2 - 3 timing.py

from timeit import Timer


class Node :
	def __init__(self) :
		self.label = None
		self.leftChild = None
		self.rightSibling = None
		self.parent = None
	def __str__(self):
                return str(self.label)

class tree :

	def __init__(self) :
		self.cellD = None
		self.root = None

def MAKENULL() :
	temp = tree()
	temp.cellD = [None]*MAXN
	return temp
def PARENT(n, T) : 
	return T.cellD[n.parent]

def LEFTMOST_CHILD(n, T) :
	if n.leftChild is None :
		return None
	else :
		return T.cellD[n.leftChild]
	print n, "We dont have child here"

def RIGHT_SIBLING(n, T):
	if n.rightSibling is None :
		return None
	else :
		return T.cellD[n.rightSibling]
	print n, "We dont have sibling here"

def LABEL(n) :
	return n.label
def CREATE0(v) :
	temp = MAKENULL()
	temp.cellD[v] = Node()
	temp.cellD[v].label = v
	temp.root = v
	return temp

def CREATE1(v, T) :
	temp = MAKENULL()
	for i in range(0, MAXN) :
		if T.cellD[i] is not None :
			temp.cellD[i] = T.cellD[i]
	temp.cellD[v] = Node()
	temp.cellD[T.root] = Node()
	temp.cellD[v].label = v
	temp.cellD[v].leftChild = T.root
	temp.cellD[v].rightSibling = None
	temp.cellD[T.root] = T.cellD[T.root]
	temp.cellD[T.root].rightSibling = None
	temp.cellD[T.root].parent = v
	temp.root = v
	return temp
	
def CREATE2(v, T1, T2) :
	temp = MAKENULL()
	for i in range(0, MAXN) :
		if T1.cellD[i] is not None :
			temp.cellD[i] = T1.cellD[i]
		elif T2.cellD[i] is not None :
			temp.cellD[i] = T2.cellD[i]
	temp.cellD[v] = Node()
	temp.cellD[T1.root] = Node()
	temp.cellD[T2.root] = Node()
	temp.cellD[v].label = v
	temp.cellD[v].leftChild = T1.root
	temp.cellD[v].rightSibling = None
	temp.cellD[T1.root] = T1.cellD[T1.root]
	temp.cellD[T1.root].rightSibling = T2.root
	temp.cellD[T1.root].parent = v
	temp.cellD[T2.root] = T2.cellD[T2.root]
	temp.cellD[T2.root].rightSibling = None
	temp.cellD[T2.root].parent = v
	temp.root = v

	return temp
	
def CREATE3(v, T1, T2, T3) :
	temp = MAKENULL()
	for i in range(0, MAXN) :
		if T1.cellD[i] is not None :
			temp.cellD[i] = T1.cellD[i]
		elif T2.cellD[i] is not None :
			temp.cellD[i] = T2.cellD[i]
		elif T3.cellD[i] is not None :
			temp.cellD[i] = T3.cellD[i]
	temp.cellD[v] = Node()
	temp.cellD[T1.root] = Node()
	temp.cellD[T2.root] = Node()
	temp.cellD[T3.root] = Node()
	temp.cellD[v].label = v
	temp.cellD[v].leftChild = T1.root
	temp.cellD[v].rightSibling = None
	temp.cellD[T1.root] = T1.cellD[T1.root]
	temp.cellD[T1.root].rightSibling = T2.root
	temp.cellD[T1.root].parent = v
	temp.cellD[T2.root] = T2.cellD[T2.root]
	temp.cellD[T2.root].rightSibling = T3.root
	temp.cellD[T2.root].parent = v
	temp.cellD[T3.root] = T3.cellD[T3.root]
	temp.cellD[T3.root].rightSibling = None
	temp.cellD[T3.root].parent = v
	temp.root = v

	return temp



def ROOT(T) :
	return T.cellD[T.root]
def printTree(n, T) :
	if n.leftChild is None :
		print n,
	else :
		printTree(T.cellD[n.leftChild], T)
		print n,
		tmp = LEFTMOST_CHILD(n,T)
		tmp = RIGHT_SIBLING(tmp, T)
		while tmp is not None :
			printTree(tmp, T)
			tmp = RIGHT_SIBLING(tmp, T)

def makeLevels(n, T, Q, i) :
	if LEFTMOST_CHILD(n, T) is None :
		if Q[i] is None :
			Q[i] = Queue()
		ENQUEUE(n.label, Q[i])
	else :
		tmp = LEFTMOST_CHILD(n, T)
		while tmp is not None :
			makeLevels(tmp, T, Q, i+1)
			tmp = RIGHT_SIBLING(tmp, T)
		if Q[i] is None :
			Q[i] = Queue()
		ENQUEUE(n.label, Q[i])

def buildTree(v, i) :
	if i == 0 :
		return CREATE0(v)
	else :
		return CREATE3(v, buildTree((3*i)-1, i-1), buildTree(3*i, i-1), buildTree((3*i)+1, i-1))

def time1() :
	T = buildTree(0, n)

def traverightSiblinge(n, T) :
	if LEFTMOST_CHILD(n, T) is None :
		return
	else :
		traverightSiblinge(LEFTMOST_CHILD(n, T), T)
		tmp = LEFTMOST_CHILD(n,T)
		tmp = RIGHT_SIBLING(tmp, T)
		while tmp is not None :
			traverightSiblinge(tmp, T)
			tmp = RIGHT_SIBLING(tmp, T)

def time2() :
	T = buildTree(0, n)
	traverightSiblinge(ROOT(T), T)

MAXN = 500
NUMREPS = 1000

T = tree()

for n in range(0,7) :

	t = Timer("time1()", "from __main__ import time1")
	buildtime = t.timeit(NUMREPS)/(NUMREPS) 
	t = Timer("time2()", "from __main__ import time2")
	travtime = t.timeit(NUMREPS)/(NUMREPS) 
	print "Time to traverightSiblinge tree", n, ":\t", travtime-buildtime