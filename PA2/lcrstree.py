# Tiancheng Zhu 8/11/2016
# PA2 - 2 lcrstree.py
class Node :
	def __init__(self) :
		self.label = None
		self.leftChild = None
		self.rightSibling = None
		self.parent = None
	
	def __str__(self):
                return str(self.label)

class tree :

	def __init__(self) :
		self.cellD = None
		self.root = None

def MAKENULL() :
	temp = tree()
	temp.cellD = [None]*maxN
	return temp

def PARENT(n, T) : 
	return T.cellD[n.parent]

def LEFTMOST_CHILD(n, T) :
	if n.leftChild is None :
		return None
	else :
		return T.cellD[n.leftChild]
	print n, "We dont have child here"

def RIGHT_SIBLING(n, T):
	if n.rightSibling is None :
		return None
	else :
		return T.cellD[n.rightSibling]
	print n, "we dont have sibling here"
		
def LABEL(n) :
	return n.label

def CREATE0(v) :
	temp = MAKENULL()

	temp.cellD[v] = Node()
	temp.cellD[v].label = v
	temp.root = v
	return temp
	
def CREATE1(v, T) :
	temp = MAKENULL()
	for i in range(0, maxN) :
		if T.cellD[i] is not None :
			temp.cellD[i] = T1.cellD[i]
	temp.cellD[v] = Node()
	temp.cellD[T.root] = Node()

	temp.cellD[v].label = v
	temp.cellD[v].leftChild = T.root
	temp.rightSibling = None
	temp.cellD[T.root].label = T.root
	temp.cellD[T.root].rightSibling = None
	temp.cellD[T.root].parent = v
	temp.root = v

	return temp

def CREATE2(v, T1, T2) :
	temp = MAKENULL()

	for i in range(0, maxN) :
		if T1.cellD[i] is not None :
			temp.cellD[i] = T1.cellD[i]
		elif T2.cellD[i] is not None :
			temp.cellD[i] = T2.cellD[i]
			
	temp.cellD[v] = Node()
	temp.cellD[T1.root] = Node()
	temp.cellD[T2.root] = Node()
	temp.cellD[v].label = v
	temp.cellD[v].leftChild = T1.root
	temp.cellD[v].rightSibling = None	# being explicit
	temp.cellD[T1.root] = T1.cellD[T1.root]
	temp.cellD[T1.root].rightSibling = T2.root
	temp.cellD[T1.root].parent = v
	temp.cellD[T2.root] = T2.cellD[T2.root]
	temp.cellD[T2.root].rightSibling = None
	temp.cellD[T2.root].parent = v
	temp.root = v
	return temp
def CREATE3(v, T1, T2, T3) :
	print "T1.root = ",T1.root
	print "T2.root = ",T2.root
	temp = MAKENULL()
	
	for i in range(0, maxN) :
		if T1.cellD[i] is not None :
			temp.cellD[i] = T1.cellD[i]
		elif T2.cellD[i] is not None :
			temp.cellD[i] = T2.cellD[i]
		elif T3.cellD[i] is not None :
			temp.cellD[i] = T3.cellD[i]

	temp.cellD[v] = Node()
	temp.cellD[T1.root] = Node()
	temp.cellD[T2.root] = Node()
	temp.cellD[T3.root] = Node()
	temp.cellD[v].label = v
	temp.cellD[v].leftChild = T1.root
	temp.cellD[v].rightSibling = None	# being explicit
	temp.cellD[T1.root] = T1.cellD[T1.root]
	temp.cellD[T1.root].rightSibling = T2.root
	temp.cellD[T1.root].parent = v
	temp.cellD[T2.root] = T2.cellD[T2.root]
	temp.cellD[T2.root].rightSibling = T3.root
	temp.cellD[T2.root].parent = v
	temp.cellD[T3.root] = T3.cellD[T3.root]
	temp.cellD[T3.root].rightSibling = None
	temp.cellD[T3.root].parent = v
	temp.root = v
	return temp



def ROOT(T) :
	return T.cellD[T.root]

def printTree(n, T) :
	if n.leftChild is None :
		print n,
	else :
		printTree(T.cellD[n.leftChild], T)
		print n,
		tmp = LEFTMOST_CHILD(n,T)
		tmp = RIGHT_SIBLING(tmp, T)
		while tmp is not None :
			printTree(tmp, T)
			tmp = RIGHT_SIBLING(tmp, T)

maxN = 100
T1= CREATE0(9)
T2 = CREATE0(19)
T3 = CREATE0(45)
T4 = CREATE0(8)
T5 = CREATE3(5, T1, T2, T3)


print "T5 (inorder): \n"
printTree(ROOT(T5), T5)
print

n = ROOT(T5)
print " ROOT(T5)\n"
print n
n = LEFTMOST_CHILD(n, T5)
print "leftmost child of ROOT(T5):\n"
print n
n = RIGHT_SIBLING(n, T5)
print "right sibling of T5: \n"
print n
n = PARENT(n, T5)
print "parent of T2:\n"
print n