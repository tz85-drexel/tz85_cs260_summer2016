# Tiancheng Zhu 8/11/2016
# PA2 - 2 pointerqueue.py

class Node:

    def __init__ (self, data = None, next = None):

        self.data = data
        self.next = next

    def __str__ (self):

        return data

class Queue:

    def __init__ (self):
    
        self.front = None

    def __str__ (self):
    
        x = self.front
        p = []
        while not x == None:
            p.append(x.data)
            x = x.next
        return str(p)


def MAKENULL (Q):
    Q.front = None

def ENQUEUE(x,Q):
	N = Q.front
	if N == None:
	    Q.front = Node (x, None)
	    return
	while not N.next == None:
	    N = N.next
	N.next = Node (x, None)
	
def DEQUEUE (Q):
    if not Q.front == None:
        Q.front =  Q.front.next
    else : print "empty"
def EMPTY (Q):
    return (Q.front == None)

def FRONT (Q):
	if EMPTY(Q):
		print "this is empty queue"
	else :
		return Q.front.data			
	

Q = Queue ()
print "front of Q:" 
print FRONT(Q)
print "Q: ", Q

print "equeue 10"
ENQUEUE (10, Q)
print "front of Q:" 
print FRONT(Q)
print "Q: ", Q

print "equeue 5"
ENQUEUE (5, Q)
print "front of Q:" 
print FRONT(Q)
print "Q: ", Q

print "dequeue"
DEQUEUE(Q)
print "Q:", Q
