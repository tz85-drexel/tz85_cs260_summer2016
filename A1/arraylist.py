class ListArray :
	def __init__(self) :
		self.data = [None]*MAXL
		self.end = 0 

def MAKENULL() :
	return ListArray()

def FIRST(L) :
	if L.end is 0 :
		return -1
	else :
		return 0

def END(L) :
	return L.end

def LOCATE(val, L) :
	for i in range(L.end) :
		if L.data[i] == val :
			return i
def RETRIEVE(p, L) :
	if p > L.end :
		return None
	else :
		return L.data[p]
def NEXT(p, L) :
	if p > L.end or p + 1 > L.end:
		return None
	else :
		return p + 1

def PREVIOUS(p, L) :
	if p > L.end or p == 0 :
		return None
	else :
		return p - 1
def INSERT(val, p, L) :	
	if p > L.end:
		return
	elif p == L.end :
		L.data[p] = val
	else :
		L.data[p+1:L.end+1] = L.data[p:L.end]
		L.data[p] = val
	L.data = L.data[:MAXL]
	L.end += 1


def DELETE(p, L) :
	if p == L.end :
		L.end -= 1 
	else :
		L.data[p:L.end-1] = L.data[p+1:L.end]
		L.end -= 1




def PRINTLIST(lst) :
	print lst.data[0:lst.end]

##############################################

MAXL = 100
foo = MAKENULL()
for i in range(8) :
	INSERT(i, 0, foo)
PRINTLIST(foo)

INSERT(3, END(foo), foo)
PRINTLIST(foo)

INSERT(15, 4, foo)
PRINTLIST(foo)

DELETE(3, foo)
PRINTLIST(foo)



p = FIRST(foo)
p = NEXT(p, foo)
print "data NO.2: ", RETRIEVE(p, foo)