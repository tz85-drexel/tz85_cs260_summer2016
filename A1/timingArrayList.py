from timeit import Timer

class ListArray :
	def __init__(self) :
		self.data = [None]*MAXL
		self.end = 0 

def MAKENULL() :
	return ListArray()

def FIRST(L) :
	if L.end is 0 :
		return -1
	else :
		return 0

def END(L) :
	return L.end

def LOCATE(x, L) :
	for i in range(L.end) :
		if L.data[i] == x :
			return i

def RETRIEVE(p, L) :
	if p > L.end :
		return None
	else :
		return L.data[p]

def NEXT(p, L) :
	if p > L.end or p + 1 > L.end:
		return None
	else :
		return p + 1

def PREVIOUS(p, L) :
	if p > L.end or p == 0 :
		return None
	else :
		return p - 1

def INSERT(x, p, L) :	
	# out of bounds
	if p > L.end:
		return
	elif p == L.end :
		L.data[p] = x
	else :
		L.data[p+1:L.end+1] = L.data[p:L.end]
		L.data[p] = x
	L.data = L.data[:MAXL]
	L.end += 1

def DELETE(p, L) :
	if p == L.end :
		L.end -= 1
	else :
		L.data[p:L.end-1] = L.data[p+1:L.end]
		L.end -= 1

def PRINTLIST(L) :
	print L.data[0:L.end]

######################################################################
def inshead() :
	L = MAKENULL()
	for i in range(0, n) :
		INSERT(5, 0, L)

def instail() :
	L = MAKENULL()
	for i in range(0, n) :
		INSERT(5, END(L), L)

def trav() :
	L = MAKENULL()
	for i in range(0, n) :
		INSERT(5, END(L), L)

	p = FIRST(L)
	while p :
		p = NEXT(p, L)

def delhead() :
	L = MAKENULL()
	for i in range(0, n) :
		INSERT(5, END(L), L)

	for i in range(0, n) :
		DELETE(0, L)

def deltail() :
	L = MAKENULL()
	for i in range(0, n) :
		INSERT(5, END(L), L)

	for i in range(0, n) :
		DELETE(END(L), L)

def assignment() :
	L = MAKENULL()

NUM = 100
for i in range(1, 4) :
	n = 8**i
	MAXL = n

	print "----- n = ", n, " ----- ArrayList"

	t = Timer("assignment()", "from __main__ import assignment")
	asgntime = t.timeit(NUM)/(NUM*n)
	
	t = Timer("inshead()", "from __main__ import inshead")
	insheadtime = t.timeit(NUM)/(NUM*n) 
	print "Time of head insertion: \t", 
	
	t = Timer("instail()", "from __main__ import instail")
	instailtime = t.timeit(NUM)/(NUM*n) 
	print "Time of tail insertion: \t", instailtime-asgntime
	
	t = Timer("trav()", "from __main__ import trav")
	travtime = t.timeit(NUM)/(NUM*n) 
	print "Time of traversal:      \t", travtime-asgntime
	
	t = Timer("delhead()", "from __main__ import delhead")
	delheadtime = t.timeit(NUM)/(NUM*n) 
	print "Time of head deletion: \t", delheadtime-asgntime
	
	t = Timer("deltail()", "from __main__ import deltail")
	deltailtime = t.timeit(NUM)/(NUM*n) 
	print "Time of tail deletion: \t", deltailtime-asgntime