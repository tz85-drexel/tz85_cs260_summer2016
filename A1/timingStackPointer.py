from timeit import Timer
class Node:
        def __init__(self):
                self.cargo = None
                self.next = None
        
        def __str__(self):
                return str(self.cargo)  
 
class StackPointer:
        def __init__(self):
                self.top = None
  
def MAKENULL() :
        temp = StackPointer()
        return temp
def POP(S) :
        S.top = S.top.next
	return

def TOP(S) :
        return S.top
 
def PUSH(x, S) :
        top = Node()
        top.cargo = x
        top.next = S.top
        S.top = top
        return 
	
def EMPTY(S) :
	if S.top is None :
		return True
	else :
		return False

def printStack(S) :
        temp = TOP(S)
        if temp is None :
                print "(empty)",
        else :
                while temp :
                        print temp,
                        temp = temp.next
        print
        
        
def PUSHT() :
	S = MAKENULL()
	for i in range(0, n) :
		PUSH(5, S)

def POPT() :
	S = MAKENULL()
	for i in range(0, n) :
		POP(S)
def assignment() :
	S = MAKENULL()
	
NUM = 100
for i in range(1, 3) :
	n = 4**i

	print "----- n = ", n, " -----   stackpointer time!!"

	t = Timer("assignment()", "from __main__ import assignment")
	asgntime = t.timeit(NUM)/(NUM*n)
	
	t = Timer("PUSHT()", "from __main__ import PUSHT")
	pushtime = t.timeit(NUM)/(NUM*n) 
	print "Time of push: \t", pushtime-asgntime
	
	t = Timer("POPT()", "from __main__ import POPT")
	poptime = t.timeit(NUM)/(NUM*n) 
	print "Time of pop: \t", poptime-asgntime