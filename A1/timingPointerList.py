from timeit import Timer
class Node:
	def __init__(self):
		self.cargo = None
		self.nxt = None
	
	def __str__(self):
		return str(self.cargo) 	

class ListPointer:
	def __init__(self):
		self.head = None
		self.cur = None

def MAKENULL() :
	temp = ListPointer()
	
	return temp

def FIRST(L) :
	return L.head

def END(L) :
	if L.head is None and L.cur is None:
		return None
	else :
		temp = L.head
		while temp.nxt:	
			temp = temp.nxt
		return temp

def LOCATE(x, L) : 
	count = 0
	temp = FIRST(L)
	while temp :
		if temp.cargo == x :
			return count
		else :
			count += 1
			temp = temp.nxt;
	return -1

def RETREIVE(p, L) :
	temp = FIRST(L)
	for k in range(0, p) :
		if temp.nxt != None :
			temp = temp.nxt
	return temp.cargo

def NEXT(n) :
	return n.nxt
	


def PREVIOUS(n, L) :
	temp = FIRST(L)
	while temp :
		if temp.nxt == n :
			return temp
		else :
			temp = temp.nxt
	return None 
	
def INSERT(x, p, L) :
	n = Node()
	n.cargo = x
	if p is None :
		n.nxt = None
		L.head = n
		L.cur = L.head
		return
	elif (p == FIRST(L) and FIRST(L) != END(L)) or (p == 0):
		n.nxt = L.head
		L.head = n
		L.cur = L.head
		return
	else :
		if L.head is None :
			L.head = n
			L.cur = n
		else :
			tmp = FIRST(L)
			while tmp and p > 1 :
				tmp = tmp.nxt
				p -= 1
			n.nxt = tmp.nxt
			tmp.nxt = n        
		return

def DELETE(p, L) :
	temp = FIRST(L)
	if p == 0 :
		L.head = temp.nxt
	elif p == END(L) :
		if temp.nxt == None :
			L = MAKENULL()
		else :
			while (temp.nxt).nxt :
				temp = temp.nxt
			temp.nxt = None
	else :
		while p - 1 > 0 :
			temp = temp.nxt
			p -= 1
		first = temp
		second = temp.nxt
		first.nxt = second.nxt
	L.cur = L.head

def printList(L) :
	temp = FIRST(L)
	if temp is None :
		print "empty",
	else :
		while temp :
			print temp,
			temp = temp.nxt
	print


def inshead() :
	L = MAKENULL()
	for i in range(0, n) :
		INSERT(i, 0, L)

def instail() :
	L = MAKENULL()
	for i in range(0, n) :
		INSERT(i, END(L), L)

def trav() :
	L = MAKENULL()
	for i in range(0, n) :
		INSERT(i, 0, L)

	tmp = FIRST(L)
	while tmp :
		tmp = NEXT(tmp)

def delhead() :
	L = MAKENULL()
	for i in range(0, n) :
		INSERT(i, 0, L)

	for i in range(0, n) :
		DELETE(0, L)

def deltail() :
	L = MAKENULL()
	for i in range(0, n) :
		INSERT(i, 0, L)

	for i in range(0, n) :
		DELETE(END(L), L)

def assignment() :
	L = MAKENULL()

NUMREPS = 100

for i in range(1, 4) :
	n = 8**i

	print "----- n = ", n, " ----- Pointer list"

	t = Timer("assignment()", "from __main__ import assignment")
	asgntime = t.timeit(NUMREPS)/(NUMREPS*n) 
	
	t = Timer("inshead()", "from __main__ import inshead")
	insheadtime = t.timeit(NUMREPS)/(NUMREPS*n) 
	print "Time of head insertion: \t", insheadtime-asgntime
	
	t = Timer("instail()", "from __main__ import instail")
	instailtime = t.timeit(NUMREPS)/(NUMREPS*n) 
	print "Time of tail insertion: \t", instailtime-asgntime

	t = Timer("trav()", "from __main__ import trav")
	travtime = t.timeit(NUMREPS)/(NUMREPS*n)
	print "Time of traversal:      \t", travtime-insheadtime

	t = Timer("delhead()", "from __main__ import delhead")
	delheadtime = t.timeit(NUMREPS)/(NUMREPS*n)
	print "Time of head deletion: \t", delheadtime-insheadtime
	
	t = Timer("deltail()", "from __main__ import deltail")
	deltailtime = t.timeit(NUMREPS)/(NUMREPS*n) 
	print "Time of tail deletion: \t", deltailtime-insheadtime