class StackArray :
	def __init__(self) :
		data = None
		end = None 
		return

def TOP(S) :
	return S.data[0]

def PUSH(x, S) :
	S.data[S.end] = x
	S.end += 1
	return

def POP(S) :
	S.end -= 1
	return

def EMPTY(S) :
	if S.end == 0 :
		return True
	else :
		return False

def MAKENULL() :
	tmp = StackArray()
	tmp.data = [0]*MAXLENGTH
	tmp.end = 0
	return tmp

def printStack(S) :
	print S.data[:S.end]
	return


MAXLENGTH = 100


print "make new stack"
foo = MAKENULL()
printStack(foo)

print "pushing 4-7"
PUSH(4, foo)
PUSH(5, foo)
PUSH(6, foo)
PUSH(7, foo)
printStack(foo)

print "pop "
POP(foo)
printStack(foo)


print "empty?"
print EMPTY(foo)

print "make empty"
foo = MAKENULL()

print "empty?"
print EMPTY(foo)