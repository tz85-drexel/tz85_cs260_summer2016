from timeit import Timer

def inshead() :
	L = [0]*n
	for i in range(0, n) :
		L.insert(0, 5)

def instail() :
	L = [0]*n
	for i in range(0, n) :
		L.append(5)
		
def trav() :
	L = [0]*n
	for i in range(len(L)) :
		x = L[i]

def delhead() :
	L = [0]*n
	for i in range(0, n) :
		del L[0]

def deltail() :
	L = [0]*n
	for i in range(0, n) :
		del L[-1]

def assignment() :
	L = [0]*n

NUM = 10000
for i in range(1, 4) :
	n = 8**i

	print "number: Library list", n

	t = Timer("assignment()", "from __main__ import assignment")
	asgntime = t.timeit(NUM)/(NUM*n)
	
	t = Timer("inshead()", "from __main__ import inshead")
	insheadtime = t.timeit(NUM)/(NUM*n) 
	print "Time of head insertion: \t", insheadtime-asgntime
	
	t = Timer("instail()", "from __main__ import instail")
	instailtime = t.timeit(NUM)/(NUM*n) 
	print "Time of tail insertion: \t", instailtime-asgntime
	
	t = Timer("trav()", "from __main__ import trav")
	travtime = t.timeit(NUM)/(NUM*n) 
	print "Time of traversal:      \t", travtime-asgntime
	
	t = Timer("delhead()", "from __main__ import delhead")
	delheadtime = t.timeit(NUM)/(NUM*n)
	print "Time of head deletion: \t", delheadtime-asgntime
	
	t = Timer("deltail()", "from __main__ import deltail")
	deltailtime = t.timeit(NUM)/(NUM*n)
	print "Time of tail deletion: \t", deltailtime-asgntime