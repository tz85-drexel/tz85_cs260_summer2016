from timeit import Timer

class ListArray :
	def __init__(self) :
		self.data = [None]*MAXL
		self.end = 0 

def MAKENULL() :
	return ListArray()

def FIRST(L) :
	if L.end is 0 :
		return -1
	else :
		return 0

def END(L) :
	return L.end

def LOCATE(val, L) :
	for i in range(L.end) :
		if L.data[i] == val :
			return i
def RETRIEVE(p, L) :
	if p > L.end :
		return None
	else :
		return L.data[p]
def NEXT(p, L) :
	if p > L.end or p + 1 > L.end:
		return None
	else :
		return p + 1

def PREVIOUS(p, L) :
	if p > L.end or p == 0 :
		return None
	else :
		return p - 1
def INSERT(val, p, L) :	
	if p > L.end:
		return
	elif p == L.end :
		L.data[p] = val
	else :
		L.data[p+1:L.end+1] = L.data[p:L.end]
		L.data[p] = val
	L.data = L.data[:MAXL]
	L.end += 1


def DELETE(p, L) :
	if p == L.end :
		L.end -= 1 
	else :
		L.data[p:L.end-1] = L.data[p+1:L.end]
		L.end -= 1




def PRINTLIST(lst) :
	print lst.data[0:lst.end]
	
	

	
def insertHead() :
	lst = MAKENULL()
	for i in range(0, n) :
		INSERT(5, 0, lst)

# INSERT TO TAIL
def insertTail() :
	lst = MAKENULL()
	for i in range(0, n) :
		INSERT(5, END(lst), lst)

# TRAVERSE
def listTraverse() :
	lst = MAKENULL()
	for i in range(0, n) :
		INSERT(5, END(lst), lst)

	p = FIRST(lst)
	while p :
		p = NEXT(p, lst)

# DELETE HEAD
def deleteHead() :
	lst = MAKENULL()
	for i in range(0, n) :
		INSERT(5, END(lst), lst)

	for i in range(0, n) :
		DELETE(0, lst)

# DELETE TAIL
def deleteTail() :
	lst = MAKENULL()
	for i in range(0, n) :
		INSERT(5, END(lst), lst)

	for i in range(0, n) :
		DELETE(END(lst), lst)

def time() :
	lst = [0]*n

EXEC = 100
for i in range(1, 4) :
	n = 10**i
	MAXNODES = n

	print "----- n = ", n, " -----"

	t = Timer("time()", "from __main__ import time")
	timeT = t.timeit(EXEC)/(EXEC*n) # NUMREPS executions * n ops
	
	t = Timer("insertHead()", "from __main__ import insertHead")
	insheadtime = t.timeit(EXEC)/(EXEC*n) # NUMREPS executions * n ops
	print "Time per head insertion: \t", insheadtime-timeT
	
	t = Timer("insertTail()", "from __main__ import insertTail")
	instailtime = t.timeit(EXEC)/(EXEC*n) # NUMREPS executions * n ops
	print "Time per tail insertion: \t", instailtime-timeT
	
	t = Timer("listTraverse()", "from __main__ import listTraverse")
	travtime = t.timeit(EXEC)/(EXEC*n) # NUMREPS executions * n ops
	print "Time per traversal:      \t", travtime-timeT
	
	t = Timer("deleteHead()", "from __main__ import deleteHead")
	delheadtime = t.timeit(EXEC)/(EXEC*n) # NUMREPS executions * n ops
	print "Time per head deletion: \t", delheadtime-timeT
	
	t = Timer("deleteTail()", "from __main__ import deleteTail")
	deltailtime = t.timeit(EXEC)/(EXEC*n) # NUMREPS executions * n ops
	print "Time per tail deletion: \t", deltailtime-timeT