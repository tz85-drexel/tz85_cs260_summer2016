class Node:
	def __init__(self):
		self.cargo = None
		self.nxt = None
	
	def __str__(self):
		return str(self.cargo) 	

class ListPointer:
	def __init__(self):
		self.head = None
		self.cur = None

def MAKENULL() :
	temp = ListPointer()
	
	return temp

def FIRST(L) :
	return L.head

def END(L) :
	if L.head is None and L.cur is None:
		return None
	else :
		temp = L.head
		while temp.nxt:	
			temp = temp.nxt
		return temp

def LOCATE(x, L) : 
	count = 0
	temp = FIRST(L)
	while temp :
		if temp.cargo == x :
			return count
		else :
			count += 1
			temp = temp.nxt;
	return -1

def RETREIVE(p, L) :
	temp = FIRST(L)
	for k in range(0, p) :
		if temp.nxt != None :
			temp = temp.nxt
	return temp.cargo

def NEXT(n) :
	return n.nxt
	


def PREVIOUS(n, L) :
	temp = FIRST(L)
	while temp :
		if temp.nxt == n :
			return temp
		else :
			temp = temp.nxt
	return None 
	
def INSERT(x, p, L) :
	n = Node()
	n.cargo = x
	if p is None :
		n.nxt = None
		L.head = n
		L.cur = L.head
		return
	elif (p == FIRST(L) and FIRST(L) != END(L)) or (p == 0):
		n.nxt = L.head
		L.head = n
		L.cur = L.head
		return
	else :
		if L.head is None :
			L.head = n
			L.cur = n
		else :
			tmp = FIRST(L)
			while tmp and p > 1 :
				tmp = tmp.nxt
				p -= 1
			n.nxt = tmp.nxt
			tmp.nxt = n        
		return

def DELETE(p, L) :
	temp = FIRST(L)
	if p == 0 :
		L.head = temp.nxt
	elif p == END(L) :
		if temp.nxt == None :
			L = MAKENULL()
		else :
			while (temp.nxt).nxt :
				temp = temp.nxt
			temp.nxt = None
	else :
		while p - 1 > 0 :
			temp = temp.nxt
			p -= 1
		first = temp
		second = temp.nxt
		first.nxt = second.nxt
	L.cur = L.head

def printList(L) :
	temp = FIRST(L)
	if temp is None :
		print "empty",
	else :
		while temp :
			print temp,
			temp = temp.nxt
	print


foo = MAKENULL()
INSERT(2, 0, foo)
INSERT(3, 0, foo)
INSERT(4, 0, foo)
INSERT(5, 0, foo)
printList(foo)


INSERT(8, 3, foo)
INSERT(7, 5, foo)
print "foo: "
printList(foo)

DELETE(1, foo)
print "deleted "
printList(foo)

DELETE(END(foo), foo)
printList(foo)

print "moving to next"
p = FIRST(foo)
p = NEXT(p)
print p

print "moving back "
p = PREVIOUS(p, foo)
print p

print "locating 11'"
print LOCATE(11, foo)

print "retreiving p 2'"
print RETREIVE(2, foo)

