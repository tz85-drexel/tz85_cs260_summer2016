class Node:
        def __init__(self):
                self.cargo = None
                self.next = None
        
        def __str__(self):
                return str(self.cargo)  
 
class StackPointer:
        def __init__(self):
                self.top = None
  
def MAKENULL() :
        temp = StackPointer()
        return temp
def POP(S) :
        S.top = S.top.next
	return

def TOP(S) :
        return S.top
 
def PUSH(x, S) :
        top = Node()
        top.cargo = x
        top.next = S.top
        S.top = top
        return 
	
def EMPTY(S) :
	if S.top is None :
		return True
	else :
		return False

def printStack(S) :
        temp = TOP(S)
        if temp is None :
                print "(empty)",
        else :
                while temp :
                        print temp,
                        temp = temp.next
        print
 
 
foo = MAKENULL()
print "push 2-4"
for i in range(2,5) :
	PUSH(i, foo)
print "foo: "
printStack(foo)
 
POP(foo)
print "pop"

printStack(foo)

print "is foo empty?"
print EMPTY(foo)

print "making foo null"
foo = MAKENULL()
printStack(foo)
print "is foo empty?"
print EMPTY(foo)
 