from timeit import Timer

class StackArray :
	def __init__(self) :
		data = None
		end = None 
		return

def TOP(S) :
	return S.data[0]

def PUSH(x, S) :
	S.data[S.end] = x
	S.end += 1
	return

def POP(S) :
	S.end -= 1
	return

def EMPTY(S) :
	if S.end == 0 :
		return True
	else :
		return False

def MAKENULL() :
	tmp = StackArray()
	tmp.data = [0]*MAXLENGTH
	tmp.end = 0
	return tmp

def printStack(S) :
	print S.data[:S.end]
	return


MAXLENGTH = 100

######################################################################
def PUSHT() :
	S = MAKENULL()
	for i in range(0, n) :
		PUSH(5, S)

def POPT() :
	S = MAKENULL()
	for i in range(0, n) :
		POP(S)
def assignment() :
	S = MAKENULL()

NUM = 100
for i in range(1, 4) :
	n = 4**i

	print "----- n = ", n, " ----- stack array time"

	t = Timer("assignment()", "from __main__ import assignment")
	asgntime = t.timeit(NUM)/(NUM*n)
	
	t = Timer("PUSHT()", "from __main__ import PUSHT")
	pushtime = t.timeit(NUM)/(NUM*n) 
	print "Time of push: \t", pushtime-asgntime
	
	t = Timer("POPT()", "from __main__ import POPT")
	poptime = t.timeit(NUM)/(NUM*n) 
	print "Time of pop: \t", poptime-asgntime
	
	